from Graphics import Square, WHITE, BLACK, GREEN, RED, BLUE, ORANGE, CYAN
from queue import PriorityQueue
import pygame

class SquareNode(Square):
    def __init__(self, pos, length, color = WHITE):
        super().__init__(pos, length, color)
        self.neighbors = []

    def make_barrier(self):
        self.color = BLACK
    def make_open(self):
        self.color = GREEN
    def make_closed(self):
        self.color = RED
    def make_path(self):
        self.color = BLUE
    def make_start(self):
        self.color = ORANGE
    def make_end(self):
        self.color = CYAN
    def clear(self):
        self.color = WHITE

    def is_barrier(self):
        return self.color == BLACK
    def is_open(self):
        return self.color == GREEN
    def is_closed(self):
        return self.color == RED
    def is_path(self):
        return self.color == BLUE
    def is_start(self):
        return self.color == ORANGE
    def is_end(self):
        return self.color == CYAN

    def update_neighbors(self, grid, line_count):
        self.neighbors = []
        if self.y < line_count - 1 and not grid[self.y + 1][self.x].is_barrier(): # DOWN
            self.neighbors.append(grid[self.y + 1][self.x])

        if self.y > 0 and not grid[self.y - 1][self.x].is_barrier(): # UP
            self.neighbors.append(grid[self.y - 1][self.x])

        if self.x < line_count - 1 and not grid[self.y][self.x + 1].is_barrier(): # RIGHT
            self.neighbors.append(grid[self.y][self.x + 1])

        if self.x > 0 and not grid[self.y][self.x - 1].is_barrier(): # LEFT
            self.neighbors.append(grid[self.y][self.x - 1])

def manhattan_distance(start, end):
    x1, y1 = start
    x2, y2 = end
    return abs(x1 - x2) + abs(y1 - y2)

def a_star(draw, grid, start, end):
    open_set = PriorityQueue()
    count = 0 # Tiebreaker incase of same distance
    open_set.put((0, count, start))
    open_set_elements = {start} # For lookup
    came_from = {} # Path
    g_scores = {spot: float("inf") for row in grid for spot in row}
    g_scores[start] = 0
    f_scores = {spot: float("inf") for row in grid for spot in row}
    f_scores[start] = manhattan_distance(start.get_pos(), end.get_pos())

    running = True
    while not open_set.empty() and running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return ({}, False)
        current = open_set.get()[2]
        open_set_elements.remove(current)
        if current == end:
            return (came_from, True)
        for neighbor in current.neighbors:
            g_score = g_scores[current] + 1
            if g_score < g_scores[neighbor]:
                came_from[neighbor] = current
                g_scores[neighbor] = g_score
                f_scores[neighbor] = g_score + manhattan_distance(neighbor.get_pos(), end.get_pos())
                if neighbor not in open_set_elements:
                    count += 1
                    open_set.put((f_scores[neighbor], count, neighbor))
                    open_set_elements.add(neighbor)
                    if neighbor != end:
                        neighbor.make_open()
        if current != start and current:
            current.make_closed()
        draw()
    return ({}, True)
