import pygame

RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GRAY = (128, 128, 128)
ORANGE = (255, 128, 0)
CYAN = (0, 255, 255)


class Square:
    def __init__(self, pos, length, color = WHITE):
        self.x, self.y = pos
        self.length = length
        self.color = color

    def draw(self, win):
        pygame.draw.rect(win, self.color, (self.x * self.length, self.y * self.length, self.length, self.length))

    def get_pos(self):
        return (self.x, self.y)
