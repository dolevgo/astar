import pygame
from Graphics import GRAY, WHITE
from AStar import SquareNode, a_star
from queue import PriorityQueue
import tkinter as tk
from tkinter import messagebox

pygame.init()

def create_grid(line_count, line_size):
    return [[SquareNode((x, y), line_size, WHITE) for x in range(line_count)] for y in range(line_count)]

def update_neighbors(grid, line_count):
    for row in grid:
        for node in row:
            node.update_neighbors(grid, line_count)

def draw_grid_lines(win, win_length, line_count, color = GRAY):
    gap = win_length // line_count
    for i in range(line_count):
        pygame.draw.line(win, color, (0, i * gap), (win_length, i * gap))
        pygame.draw.line(win, color, (i * gap, 0), (i * gap, win_length))

def draw(win, win_length, line_count, grid):
    win.fill(WHITE)
    for row in grid:
        for square in row:
            square.draw(win)
    draw_grid_lines(win, win_length, line_count, GRAY)
    pygame.display.update()

def click_to_grid_coord(pos, gap):
    return tuple(elem // gap for elem in pos)

def get_clicked_coord(gap):
    return click_to_grid_coord(pygame.mouse.get_pos(), gap)

def reconstruct_path(win, path, current, draw):
    while current in path:
        current = path[current]
        current.make_path()
        draw()

def clear_aStar_remains(grid):
    for row in grid:
        for node in row:
            if node.is_open() or node.is_closed():
                node.clear()

def errorMessageBox(message, title="Error"):
    root = tk.Tk()
    root.overrideredirect(1)
    root.withdraw()
    messagebox.showerror(title, message)
    root.destroy()

def main():
    pygame.display.set_caption("AStar Path Finding")
    WIN_DIMENSION = 800
    LINE_COUNT = 40
    GRID_GAP = WIN_DIMENSION // LINE_COUNT

    WIN = pygame.display.set_mode((WIN_DIMENSION, WIN_DIMENSION))

    grid = create_grid(LINE_COUNT, GRID_GAP)
    start, end = None, None
    running = True
    draw_func = lambda: draw(WIN, WIN_DIMENSION, LINE_COUNT, grid)
    draw_func()
    while running:
        draw(WIN, WIN_DIMENSION, LINE_COUNT, grid)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_SPACE:
                    if start and end:
                        update_neighbors(grid, LINE_COUNT)
                        path, finished = a_star(draw_func, grid, start, end)
                        if finished and path:
                            reconstruct_path(WIN, path, end, draw_func)
                            start.make_start()
                        else:
                            if not finished:
                                errorMessageBox("Pathfinding terminated")
                            else:
                                errorMessageBox("Path not found")
                        clear_aStar_remains(grid)
                    else:
                        errorMessageBox("You need to provide start and end points")

                elif event.key == pygame.K_r:
                    grid = create_grid(LINE_COUNT, GRID_GAP)
                    start, end = None, None

            if pygame.mouse.get_pressed()[0]: # LEFT
                x, y = get_clicked_coord(GRID_GAP)
                pressed = grid[y][x]
                if not start and not pressed.is_end():
                    start = pressed
                    pressed.make_start()
                elif not end and not pressed.is_start():
                    end = pressed
                    pressed.make_end()
                elif not pressed.is_start() and not pressed.is_end():
                    pressed.make_barrier()
            elif pygame.mouse.get_pressed()[2]: # RIGHT
                x, y = get_clicked_coord(GRID_GAP)
                pressed = grid[y][x]
                if pressed == start:
                    start = None
                elif pressed == end:
                    end = None
                pressed.clear()
    pygame.quit()
if __name__ == '__main__':
    main()
